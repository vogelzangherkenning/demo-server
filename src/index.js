var express = require('express'),

	// Tom Knapen's DTW code
	warping = require('warping'),

	// HnSRTree library
	hnsrtree = require('hnsrtree'),

	// Our index file
	srindex = new hnsrtree.HnSRIndex('birds.out'),

	// Database
	Pool = require('ibm_db').Pool,
	db = new Pool(), // Create a connectionpool

	// Node.JS builtin ( FileSystem and fork )
	fs = require('fs'),
	cp = require('child_process'),

	birds = require('./birds.js'),

	// Our HTTP server using express.js
	server = express(),

	// In-memory datastore for recorded audio files. ( only used for debugging, is not persistent )
	datastore = {
		
		_position: 0,

		getFreePosition: function() {
			while( datastore[ this._position ] )
				this._position++;
			return this._position;
		}

	},

	// Configuration options
	options = {
		connectionString: "DRIVER={DB2};DATABASE=birdshaz;HOSTNAME=localhost;UID=tweety;PWD=pr0j3ct;PORT=50000;PROTOCOL=TCPIP",
		dimensions: parseInt(process.env['DIMENSIONS'])||100, // The number of dimensions to reduce to
		neighbors: parseInt(process.env['NEIGHBORS'])||100, // The number of neighbours to find
		dtwBandWidth: Math.round(this.dimensions * 0.8), // DTW bandwidth parameter, should match enveloppe R-value ( which is hardcoded in node-hnsrtree as #dimensions * 0.8 )

		respondTop: 10, // The number of results in the response

		respondWithTiming: false, // Add spectogram to response
		respondWithSignature: false, // Add reduced sequence ( aka signature ) to response

		minWeight: 0.70, // The minimum weight of a pathcost in the average of a bird
		matchGain: 1000 // Points awarded for each match ( these are used as margin )
	},

	// Run parse.R on the audio file.
	parseAudioFile = function( file, cb ) {
		var p = cp.spawn( 'Rscript', ['parse.R', file, file + '.txt'] );

		p.on('close', function(code) {
			if( code !== 0 )
				return cb('non-zero exit');

			return parseMatrixFile( file + '.txt', cb );
		});
	},

	// Read the output txt from parse.R
	parseMatrixFile = function( file, cb ) {
		fs.readFile( file, { 'encoding': 'utf8' }, function( err, data ) {
			if( err ) return cb( err );

			var regex = /(\d+(\.\d+)?)\s(\d+(\.\d+)?)\n/g,
				d = regex.exec( data ),
				result = [];

			while( d !== null ) {
				result.push({
					time: parseFloat(d[1])*1000,
					frequency: parseFloat(d[3])*1000
				});
				d = regex.exec( data )
			}

			return cb( null, result );
		});
	},

	getSequence = function( spectrogram ) {
		var seq = [],
			i = null;

		for( i in spectrogram )
			seq.push(spectrogram[i][1]);

		return seq;
	},

	// Use dynamic timewarping to rank results
	rankResults = function( sequence, samples ) {
		var seq = null,
			i = null,
			q1 = null,
			q2 = null,
			birdId = null,
			pathCost = null,
			b = null,
			birds = {},
			count = samples.length,
			sampleId = 1;

		for( i in samples ) {
			i = samples[i];

			// Spectogram has not been read yet.
			if( !i['SPECTROGRAM'] )
				continue;

			birdId = i['BIRDID'];

			if( !birds[birdId] ) {
				birds[birdId] = i;
				birds[birdId].matches = 0;
				birds[birdId].sumPathCost = 0;
			}

			b = birds[birdId];

			seq = getSequence( JSON.parse(i['SPECTROGRAM']) );
			if( seq.length < sequence.length ) {
				q1 = seq; q2 = sequence;
			} else {
				q1 = sequence; q2 = seq;
			}
			pathCost = warping.pathCost( q1, q2, options.dtwBandWidth );

			if( !b.bestPathcost || b.bestPathcost > pathCost )
				b.bestPathcost = pathCost;
			
			// Attach a weight to results according to where they were found.
			b.sumPathCost += ( pathCost * ( options.minWeight + ( ( 1 - options.minWeight) * ( count / ( sampleId++ * 100 ) ) ) ) );
			b.matches++;

			if( b.SPECTROGRAM ) {
				delete b.SPECTROGRAM;
				delete b.REDUCEDSEQUENCE;
				delete b.SAMPLEPATH;
				delete b.SAMPLEID;
			}
		}

		for( i in birds )
			birds[i].averagePathcost = ( birds[i].sumPathCost / birds[i].matches );

		return birds;
	},

	// Sort results
	getBestResults = function( results ) {
		var top = [],
			i = null,
			j = null,
			t = null;

		// Sort on averagePathcost
		for( i in results ) {
			i = results[i];

			for( j=0; j<options.respondTop; j++ ) {
				if( !top[j] || top[j].averagePathcost > i.averagePathcost ) {
					top.splice( j, 0, i );
					break;
				}
			}

			top = top.slice( 0, options.respondTop );
		}

		// Rank results with lower averagePathcost, but more matches higher.
		for( i=0; i<top.length; i++ ) {
			for(j=i; j<top.length; j++ ) {
				if( i == j ) continue;

				// Allow for a difference of options.matchGain * the number of matches more on pathcost
				t = Math.abs( top[j].matches - top[i].matches ) * options.matchGain;

				if( top[j].averagePathcost > top[i].averagePathcost &&
					top[j].averagePathcost - top[i].averagePathcost < t &&
					top[j].matches > top[i].matches ){

					// Swap positions since we believe top[j] is a better match.
					t = top[j];
					top[j] = top[i];
					top[i] = t;
					i--; // redo loop
					break;
				}
			}
		}

		return top;
	}

	// Handlers of POST request based on content-type
	// Will run default if content-type is not found.
	handlePost = {
		'default': function( req, resp ) {
			var pos = datastore.getFreePosition(),
				type = req.get('content-type'),
				file = 'songs/' + pos + '.wav';

			// Temporary placeholder
			datastore[pos] = 'taken';

			// Write raw body to disk for parseAudioFile
			fs.writeFile(file, req.raw, function(err) {
				if( err ) {
					datastore[pos] = null;
					return error( resp, 500, 'Internal Server Error', 'Could not store audio file.' );
				}

				datastore[pos] = {
					filename: file,
					type: type,
					length: req.get('content-length'),
					meta: 'unavailable'
				};

				parseAudioFile( file, function( err, meta ) {
					var i = null,
						sequence = [],
						reduced = null,
						keys = null;

					// If an error occurred
					if( err )
						return error( resp, 500, 'Could not parse audio file.', err );

					// Read the frequency data from the spectrogram information
					for( i in meta )
						sequence.push( meta[i].frequency );
				
					// Redueced sequence
					reduced = warping.reduce( sequence, options.dimensions );

					// De keys for the neighbors neirest to reduced.
					keys = srindex.getNeighbors( reduced, options.neighbors );

					db.open(options.connectionString, function (err,conn) {
						if (err)
							return console.log(err);

						conn.query('select * from birdshaz.birdsamples s, birdshaz.birds b ' +
							'WHERE s.birdid = b.birdid AND s.sampleid IN (' + keys.join(',') + ')', [], function (err, data) {

							// Close connection to DB2
							conn.close(function(){});

							datastore[pos].birds = rankResults(sequence, data);
							datastore[pos].birds = getBestResults( datastore[pos].birds );

							resp.status = 200;
							resp.end(JSON.stringify({
								success: true,
								id: pos,
								type: type,
								length: req.get('content-length'),
								filename: file,
								meta: datastore[pos].meta,
								birds: datastore[pos].birds || {
									generated: true,
									name: birds[ Math.floor( Math.random() * birds.length ) ]}
							}));
						});
					});

					// Meta data in HTTP response
					datastore[pos].meta = {};

					// Add spectogram to HTTP response
					if( options.respondWithTiming ) {
						datastore[pos].meta.timing = {
							description: {
								tifme: 'Time at which sample was taken in msec',
								frequency: 'The maximum frequency in this sample in Hz'
							},
							data: meta
						};
					}

					// Add signature to HTTP response
					if( options.respondWithSignature ) {
						datastore[pos].meta.signature = {
							signature: {
								description: 'Reduced frequency sequence.',
								data: reduced
							},
							matchingKeys: keys
						};
					}
				});
			});	
		},

		'application/x-www-form-urlencoded': function( req, resp ) {
			if( !req.body.id )
				return error( resp, 400, 'Bad Request', 'POST data does not contain an ID.' );

			datastore[ req.body.id ] = req.body;
			resp.status = 200;
			resp.end(JSON.stringify({
				success: true
			}));
		}
	},

	error = function( resp, code, message, detail ) {
		resp.status = code;
		resp.end(JSON.stringify({
			success: false,
			message: message,
			detail: detail
		}));
	};

// Read full request body before continuing.
server.use(function( req, resp, next ) {
	req.raw = new Buffer(0);
	req.on('data', function(chunk){
		if( typeof chunk === 'string' )
			chunk = new Buffer(chunk);
		req.raw = Buffer.concat([
			req.raw,
			chunk]);
	});
	req.on('end', next);
});

// Parse body for known content-types
server.use(express.bodyParser());
server.use(server.router);
server.use(function( req, resp, next ) {
	return error( resp, 404, 'Not Found', 'The endpoint ' + req.method + ' ' + req.url + ' does not exist.' );
});

// Request endpoints
server.get( '/audio/:id/playback', function( req, resp ) {
	if( !req.params.id )
		return error( resp, 400, 'Bad Request', 'ID is missing, request should be of form /audio/:id' );

	if( !datastore[ req.params.id ] )
		return error( resp, 404, 'Not Found', 'This ID was not present in our datastore.' );

	var data = datastore[req.params.id];

	resp.status = 200;
	if( data.filename && data.type ) {
		fs.readFile( data.filename, function( err, buffer ){
			if( err )
				return error( resp, 500, 'Internal Server Error', 'Audio file could not be retrieved.' );

			resp.set('Content-Type', data.type );
			resp.set('X-Reported-Size', data.length );
			resp.set('Content-Length', data.length );
			resp.end( buffer );
		});
	} else {
		resp.end(JSON.stringify(data));
	}
});

server.get( '/audio/:id', function( req, resp ) {
	if( !req.params.id )
		return error( resp, 400, 'Bad Request', 'ID is missing, request should be of form /audio/:id' );

	if( !datastore[ req.params.id ] )
		return error( resp, 404, 'Not Found', 'This ID was not present in our datastore.' );

	var data = datastore[req.params.id];

	resp.status = 200;
	resp.end(JSON.stringify(data));
});

server.post( '/compare/:sampleid', function( req, resp ) {
	if( !req.params.sampleid )
		return error( resp, 400, 'Bad Request', 'Sample ID is missing, include a sampleid to compare fragment with.');

	var data = {
			fragment: null,
			sample: null
		},
		pos = datastore.getFreePosition(),
		file = 'songs/' + pos + '.wav';

	fs.writeFile(file, req.raw, function(err) {
		if( err )
			return error( resp, 500, 'Internal Server Error', 'Could not store audio file.' );

		parseAudioFile( file, function( err, meta ) {
			if( err )
				return error( resp, 500, 'Internal Server Error', 'Error parsing file' );

			var seq = [],
				i = null,
				output = {};

			for( i in meta )
				seq.push( meta[i].frequency );

			output.fragment = {
				//spectrogram: seq,
				reduced: warping.reduce( seq, options.dimensions )
			};

			db.open(options.connectionString, function (err,conn) {
				if (err)
					return console.log(err);

				conn.query('select * from birdshaz.birdsamples s, birdshaz.birds b ' +
					'WHERE s.birdid = b.birdid AND s.sampleid = ?', [ req.params.sampleid ], function (err, data) {

					// Close connection to DB2
					conn.close(function(){});

					if( data.length < 1 )
						return error( resp, 404, 'Not Found', 'The sample with id ' + req.params.sampleid + ' could not be found.' );

					var seq1 = [],
						i = null,
						s = JSON.parse(data[0].SPECTROGRAM);

					for( i in s )
						seq1.push( s[i][1] );

					output.sample = {
						//spectrogram: s,
						reduced: warping.reduce( seq1, options.dimensions )
					};

					output.pathCost = warping.pathCost( seq, seq1, options.dtwBandWidth );

					resp.end(JSON.stringify(output));
				});
			});
		});
	});
});

server.post( '/audio', function( req, resp ) {
	var type = req.get('content-type');
	if( !handlePost[ type ] )
		type = 'default';

	handlePost[ type ]( req, resp );
});

server.delete( '/audio/:id', function( req, resp ) {
	if( !req.params.id )
		return error( resp, 400, 'Bad Request', 'ID is missing, request should be of form /audio/:id' );

    if( !datastore[ req.params.id ] )
    	return error( resp, 404, 'Not Found', 'This ID was not present in our datastore.' );

	delete datastore[ req.params.id ];

    resp.status = 200;
	resp.end(JSON.stringify({ success: true }));
});

server.listen( process.env['PORT']||8080 );
