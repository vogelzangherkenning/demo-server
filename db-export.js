var fs = require('fs'),
	db = require('ibm_db'),

	cp = require('child_process'),

	dtw = require('warping'),
	output = fs.createWriteStream('birds.records'),

	dimensions = 100,

	parseAudioFile = function( file, cb ) {
		var p = cp.spawn( 'Rscript', ['parse.R', file, file + '.txt'] );

		p.on('close', function(code) {
			if( code !== 0 )
				return cb('non-zero exit');

			return parseMatrixFile( file + '.txt', cb );
		});
	},

	parseMatrixFile = function( file, cb ) {
		file = file.replace('~/', '/home/tweety/');
		//console.log( 'parsing', file );
		fs.readFile( file, { 'encoding': 'utf8' }, function( err, data ) {
			if( err ) return cb( err );

			var regex = /(\d+(\.\d+)?)\s(\d+(\.\d+)?)\n/g,
				d = regex.exec( data ),
				result = [];

			while( d !== null ) {
				result.push({
					time: parseFloat(d[1])*1000,
					frequency: parseFloat(d[3])*1000
				});
				d = regex.exec( data )
			}

			return cb( null, result );
		});
	},

	parseSample = function( item, cb ) {
		var path = item['SAMPLEPATH'],
			id = item['SAMPLEID'];


		//parseAudioFile( path, function( err, data ) {
			var arr = [],
				i = 0,
				reduced = null,
				data = JSON.parse(item['SPECTROGRAM']);

			if( !item.SPECTROGRAM )
				return setImmediate(cb);

			for( i in data )
				arr.push( data[i][1] );

			reduced = dtw.reduce( arr, dimensions );

			output.write( reduced.join(':') + ':' + id + "\n" );

			setImmediate(cb);
		//});
	};

output.write( dimensions + "\n" );

db.open("DRIVER={DB2};DATABASE=birdshaz;HOSTNAME=localhost;UID=tweety;PWD=pr0j3ct;PORT=50000;PROTOCOL=TCPIP", function (err,conn) {
  if (err) return console.log(err);

  conn.query('select * from birdshaz.birdsamples', [], function (err, data) {
    if (err) console.log(err);

    var i = 0,
    	keys = [],
    	cb = function() {
    		i++;
    		if( i >= keys.length ) {
    			output.end();
    			return;
    		}
    		console.log( 'parsing item ' + keys[i] );
    		parseSample( data[keys[i]], cb );
    	}

    for( i in data ) {
    	keys.push( i );
    }

    conn.close(function(){});
    i = 0;
    parseSample( data[keys[i]], cb );

  });
});
