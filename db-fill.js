var fs = require('fs'),
	pool = require('ibm_db').Pool,
	db = new pool(),

	cp = require('child_process'),

	dtw = require('warping'),
	//output = fs.createWriteStream('birds.index'),

	connectionString = "DRIVER={DB2};DATABASE=birdshaz;HOSTNAME=localhost;UID=tweety;PWD=pr0j3ct;PORT=50000;PROTOCOL=TCPIP",

	dimensions = 16,

	parseAudioFile = function( file, cb ) {
		var p = cp.spawn( 'Rscript', ['parse.R', file, file + '.txt'] );

		p.on('close', function(code) {
			if( code !== 0 )
				return cb('non-zero exit');

			return parseMatrixFile( file + '.txt', cb );
		});
	},

	parseMatrixFile = function( file, cb ) {
		file = file.replace('~/', '/home/tweety/');
		//console.log( 'parsing', file );
		fs.readFile( file, { 'encoding': 'utf8' }, function( err, data ) {
			if( err ) return cb( err );

			var regex = /(\d+(\.\d+)?)\s(\d+(\.\d+)?)\n/g,
				d = regex.exec( data ),
				result = [];

			while( d !== null ) {
				result.push([
					parseFloat(d[1])*1000, // time
					parseFloat(d[3])*1000 // frequency
				]);
				d = regex.exec( data )
			}

			return cb( null, result );
		});
	},

	parseSample = function( item, cb ) {
		var path = item['SAMPLEPATH'],
			id = item['SAMPLEID'];

		parseAudioFile( path, function( err, data ) {
			if( err ) {
				setImmediate(cb);
				return console.error( 'missing file [' + path + ']', err );
			}
			var arr = [],
				i = 0,
				reduced = null;

			for( i in data )
				arr.push( data[i][1] );

			reduced = dtw.reduce( arr, dimensions );

			db.open(connectionString, function( err, conn ) {
				if( err ) return console.error( err );

				console.log( 'json length is ', JSON.stringify(data).length );
				conn.query( 'UPDATE birdshaz.birdsamples SET spectrogram = ?, reducedSequence=? WHERE sampleID=?', [JSON.stringify(data),JSON.stringify(reduced),id], function( err, data ) {
					conn.close(function(){});
					if( err )
						return console.log( 'Update failed', err );
					cb();
				});
			});
		});
	};

//output.write( dimensions + "\n" );

db.open(connectionString, function (err,conn) {
  if (err) return console.log(err);

  conn.query('select * from birdshaz.birdsamples', [], function (err, data) {

    if (err) console.log('init',err);

    var i = 0,
    	keys = [],
    	activeParsers = 0,
    	MAX_PARSERS = 10,
    	cb = function() {
    		i++;

    		if( i >= keys.length ) {
    			//output.close();
    			return;
    		}

    		if( data[keys[i]]['SPECTROGRAM'] && data[keys[i]]['SPECTROGRAM'].replace(/\s*/g, '') )
    			return setImmediate(cb);

    		console.log( 'parsing item ' + keys[i] );
    		
    		activeParsers++;
    		parseSample( data[keys[i]], function(){
    			activeParsers--;
    			setImmediate(cb);
    		});
    		if( activeParsers < MAX_PARSERS )
    			setImmediate(cb);
    	}

    for( i in data ) {
    	keys.push( i );
    }

    conn.close(function(){});

    i = 0;
    parseSample( data[keys[i]], cb );

  });
});
